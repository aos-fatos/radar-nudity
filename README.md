Radar Nudity
======


About
-----

Nudity detection with re-trained Tensorflow MobileNet Model. Accuracy is
92.2% based on my dataset.


Requirements
------------

-   Python3.5+

Usage
-----

via command-line

``` {.sourceCode .sh}
$ nudityradar --image=IMAGE_FILE
```

via Python Module

``` {.sourceCode .python}
from nudityradar import Nudity
nudityradar = Nudity();
print(nudityradar.has('/file/path/example.jpg'))
# gives you True or False

print(nudityradar.score('/file/path/example.jpg'))
# gives you nudity score 0 - 1
```
